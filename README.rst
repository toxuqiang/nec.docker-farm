installation & usage
==========================



* install docker

* install python packages from `conf.d/requirements.txt`

* assign a **unique namespace** variable at `conf.d/local.yml`

* generate and add the ssh keys in 'conf.d/local.yml' under SSH:PUBLIC_KEYS.

* build basic docker images -- `baseimage` and `openjdk-7-jdk`::
    
    fab -f setup.py -R localhost build_init_images

* build extra images -- `kafka`, `elasticsearch`, `spark`::

    fab -f setup.py -R localhost build_images

* build containers::

    fab -f setup.py -R localhost build_containers

* populate hostnames within docker cluster::

    fab -f setup.py -R localhost build_hostnames
    fab -u root -f setup.py -R containers populate_hostnames 

* populate ssh keys within docker cluster::

    fab -u root -f setup.py -R containers build_ssh_keys
    fab -u root -f setup.py -R containers populate_ssh_keys 

* clone ngla-stable and launch::

    fab -u root -f setup.py -R containers clone
    fab -u root -f setup.py -R containers launch


advanced features
==========================

* to add more containers, update/add `conf.d/local.yml` as follows, with `key` as the unique name of the container, and `value` as the image name of container. The image names must be one of the listed dockerfiles in `dockerfiles/templates` directory. (p.s. refer to `conf.d/default.yml` as an example.)::

    CONTAINERS: 
        kafka: 
            image: 'kafka-0.9.0.1'
            order: 1
            type: 'kafka'
        elasticsearch: 
            image: 'elasticsearch-1.3.1'
            order: 2
            type: 'elasticsearch'
        spark-master: 
            image: 'spark-1.6.1'
            order: 3
            type: 'spark-master'
        spark-slave: 
            image: 'spark-1.6.1'
            order: 4
            type: 'spark-slave'
        ngla-1: 
            image: 'openjdk-7-jdk'
            order: 5
            type: 'ngla'
        ngla-2: 
            image: 'openjdk-7-jdk'
            order: 6
            type: 'ngla'

* to customize hostname patterns, update/add `conf.d/local.yml` as follows. (p.s. refer to `conf.d/default.yml` as an example.)::

    TEMPLATE:
        HOSTNAME: $container_id.$namespace.ngla.asds.nec-labs.com

* to add ssh key, vimrc string, or screenrc string, update/add `conf.d/local.yml` as follows. (p.s. refer to `conf.d/default.yml` as an example.):: 

    NAMESPACE: test
    SSH: 
        PUBLIC_KEYS:
            - "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAoaF+mpqjAt6RY1QcDdfoxjUGQ7ba0aHSy279XcKS/Mesv2/Cm6b7sQIJhDKIfaLrZDVeTy2wKLglUXgWEd9Z3bAgwB3uqjliC6KfWgzwXuTlpcuJq991/1t4u1lamv5lNNz/esL6hUy7hQsv0GnMWo4D8KItBbJn/qKVOaQ9VGKuDx8u6qumj4gT6ndLH/XpJlQj3+ekCOIEI1OS7dp+HNIf4rpzrqoMxD2JPD1yfpzc5j6vincfkqxTyIczsbVqPh62lUp+Eg6o7g2bRRH94Om3A/JkxCKrzXKCYDzf35Atv1VTEXR739PCr05Lk7IMSDC4uq1wxUev3/1W6Flw3w== qiangxu@zektor.gpcc.itd.umich.edu"
            - "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC2q4NpMwzsRnG1URlqQ8fC7ZAhPe+qtB1czNXjunsdb8dbYQMHXIzCiNZeeRrzOqG514/BLzlLcpYvPt1OmLCscFo9JoNnzxmxCLRFq9CTEIeYTfB+Z1z5om9EdQl5f60tK+fnXX2h/+tssTCxgafVy8hIQvadqgA61ofspmy+cpANzeWj4kzn/ijtBow9bTAHy2BnF3yiSeh6W/De1sY8kx/v2RuvifkaW/XN6V2nK7sep42d7+ZN00Fq6/FqaMUzbAsLWhW4pYyxzt/vd9AiHLjy9XVQT9Buof/wwlnvnHeCJvWrf//PxINCl46yOJ7Eoptm6qEUPOIkOC+zezzD root@khaleesi"


* in case you want to cleanup:: 

    fab -f setup.py -R localhost cleanup_containers
    fab -f setup.py -R localhost cleanup_images

* docker cheat sheet, https://github.com/wsargent/docker-cheat-sheet

be careful with the following things
==========================

* don't change `dockerfiles`

* don't change `conf.d/default.yml`


docker networking example
==========================

config dedicated public ip::

    sudo ip link add virtual0 link eth0 type macvlan mode bridge
    sudo ip address add 192.168.3.115/24 broadcast 192.168.3.255 dev virtual0
    sudo ip link set virtual0 up
    sudo iptables -t nat -N BRIDGE-VIRTUAL0
    sudo iptables -t nat -A PREROUTING -p all -d 192.168.3.115 -j BRIDGE-VIRTUAL0
    sudo iptables -t nat -A OUTPUT -p all -d 192.168.3.115 -j BRIDGE-VIRTUAL0
    sudo iptables -t nat -A BRIDGE-VIRTUAL0 -p all -j DNAT --to-destination 172.17.0.2
    sudo iptables -t nat -I POSTROUTING -p all -s 172.17.0.2 -j SNAT --to-source 192.168.3.115

cleanup the above configuration::

    #sudo ip link del virtual0
    #sudo ip link set virtual0 down
    #iptables -t nat -F
    #iptables -t nat -F BRIDGE-VIRTUAL0

