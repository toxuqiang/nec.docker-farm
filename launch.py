from fabric.api import *
from fabric.context_managers import cd, settings
from fabric.contrib.files import exists, append, comment
from functools import wraps
import anyconfig
from pydash import py_
import termcolor
import ast
import datetime
import simplejson
import os
import json
import random
import re
import shutil
import string
import subprocess
import sys
import time
import uuid
import yaml


from setup import *

@roles('containers')
def launch(*args, **kwargs):
    #import pdb; pdb.set_trace()
    if len(args) != 0: 
        container_ids = list(args)
    else: 
        container_ids = get_container_ids()

    #import pdb; pdb.set_trace()
    container_id = get_container_id(env.host_string)
    if container_id in config['CONTAINERS'].keys() and container_id in container_ids: 
        if config['CONTAINERS'][container_id]['type'] == 'kafka': 
            with settings(warn_only=True):
                run("ps axu | grep kafka-server-start | grep -v grep | awk '{print $2;}' | xargs kill -s 9")
                run("ps axu | grep java | grep kafka | grep -v grep | awk '{print $2;}' | xargs kill -s 9")
                run("service zookeeper status; service zookeeper restart; sleep 10")

            #runbg("bash `ls /kafka*/bin/kafka-server-start.sh` `ls /kafka*/config/server.properties` --override delete.topic.enable=true")
            runbg("bash `ls /kafka*/bin/kafka-server-start.sh` `ls /kafka*/config/server.properties`")
        
        if config['CONTAINERS'][container_id]['type'] == 'elasticsearch': 
            with settings(warn_only=True):
                run("ps axu | grep elasticsearch | grep -v grep | awk '{print $2;}' | xargs kill -s 9")
                run("ps axu | grep java | grep elasticsearch | grep -v grep | awk '{print $2;}' | xargs kill -s 9")

            runbg("bash `ls /elasticsearch*/bin/elasticsearch` --Xmx 1G --cluster.name=%s --network.host=0.0.0.0" % get_elasticsearch_cluster())

        if config['CONTAINERS'][container_id]['type'] == 'spark-master': 
            with settings(warn_only=True):
                run("ps axu | grep java | grep spark | grep -v grep | awk '{print $2;}' | xargs kill -s 9")
        
            env.spark_master_id = container_id
            runbg('bash `ls /spark*/sbin/start-master.sh`')

        if config['CONTAINERS'][container_id]['type'] == 'spark-slave': 
            with settings(warn_only=True):
                run("ps axu | grep java | grep spark | grep -v grep | awk '{print $2;}' | xargs kill -s 9")

            runbg('bash `ls /spark*/sbin/start-slave.sh` spark://%s:7077' % get_hostname(env.spark_master_id))

        if config['CONTAINERS'][container_id]['type'] == 'spark-gulzar-master': 
            with settings(warn_only=True):
                run("ps axu | grep java | grep spark | grep -v grep | awk '{print $2;}' | xargs kill -s 9")
        
            env.spark_master_id = container_id
            runbg('bash `ls /spark*/sbin/start-master.sh`')

        if config['CONTAINERS'][container_id]['type'] == 'spark-gulzar-slave': 
            with settings(warn_only=True):
                run("ps axu | grep java | grep spark | grep -v grep | awk '{print $2;}' | xargs kill -s 9")

            runbg('bash `ls /spark*/sbin/start-slave.sh` spark://%s:7077' % get_hostname(env.spark_master_id))

def get_elasticsearch_cluster(): 
    return string.Template(config['TEMPLATE']['ELASTIC_CLUSTER']).safe_substitute(namespace=config['NAMESPACE'])


@roles('containers')
def stat(): 
    container_id = get_container_id(env.host_string)
    ps = {
        'zookeeper': "ps axu | grep java | grep zookeeper | grep -v grep",        
        'kafka': "ps axu | grep java | grep kafka | grep -v grep", 
        'elasticsearch': "ps axu | grep java | grep elastic | grep -v grep", 
        'spark:slave': "ps axu | grep java | grep org.apache.spark.deploy.work | grep -v grep", 
        'spark:master': "ps axu | grep java | grep org.apache.spark.deploy.master | grep -v grep", 
            }
   
    pss = {}
    for k in ps.keys(): 
        with hide('status', 'aborts', 'warnings', 'running', 'everything', 'stdout', 'stderr', 'user'):
            output = run(ps[k] + " | wc -l")
            pss[k] = output

    print termcolor.colored(container_id, 'red'), json.dumps(pss).replace("1", termcolor.colored("1", 'green'))


if __name__ == '__main__':
    print '\n'.join([
        'fab -u root -f %s -R containers launch' % sys.argv[0]
    ])
