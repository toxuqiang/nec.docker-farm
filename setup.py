#!/usr/bin/env python
# CREATED DATE: Mon 18 Apr 2016 02:53:50 PM EDT
# CREATED BY: qiangxu, toxuqiang@gmail.com

from fabric.api import *
from fabric.context_managers import cd, settings
from fabric.contrib.files import exists, append, comment
from functools import wraps
import anyconfig
from pydash import py_
import termcolor
import ast
import datetime
import simplejson
import os
import json
import random
import re
import shutil
import string
import subprocess
import sys
import time
import uuid
import yaml
import jinja2

#import pdb; pdb.set_trace()

try:
    config = yaml.load(file("conf.d/default.yml").read())
except Exception as e:
    print e
    sys.exit(0)

try:
    config.update(yaml.load(file("conf.d/local.yml").read()))
except Exception as e:
    pass

if 'NAMESPACE' not in config.keys():
    print "set NAMESPACE in `conf.d/local.yml`"
    sys.exit(0)


e = jinja2.Environment()
env.roledefs = {
    'localhost': ['localhost'],
    #'containers': [get_hostname(container_id) for container_id in get_container_ids()]
}
env.reject_unknown_hosts = False 

def check_environment():
    for r in ['docker']:
        try:
            output = subprocess.check_call(
                ['which', r], stdout=open(os.devnull, 'w'))
        except Exception as e:
            print "install docker: http://docs.docker.com/engine/installation/linux/ubuntulinux/"

    return True


def copy_authorized_keys(path):
    with open('%s/authorized_keys' % path, 'w') as f:
        f.write('\n'.join(config['SSH']['PUBLIC_KEYS']))


def delete_authorized_keys(path):
    print_popen(subprocess.Popen("rm %s/authorized_keys" %
                                 (path), shell=True, stdout=subprocess.PIPE))


def copy_rc_files(path):
    if 'RC' in config.keys(): 
        if 'VIM' in config['RC'].keys(): 
            with open('%s/vimrc' % path, 'w') as f:
                f.write(config['RC']['VIM'])

        if 'SCREEN' in config['RC'].keys(): 
            with open('%s/screenrc' % path, 'w') as f:
                f.write(config['RC']['SCREEN'])


def delete_rc_files(path):
    print_popen(subprocess.Popen("rm %s/vimrc" %
                                 (path), shell=True, stdout=subprocess.PIPE))
    print_popen(subprocess.Popen("rm %s/screenrc" %
                                 (path), shell=True, stdout=subprocess.PIPE))


def init_image_dir(image_id):
    image_dir = get_image_dir(image_id)
    image_template_dir = get_image_template_dir(image_id)

    print_popen(subprocess.Popen("mkdir -p %s" %
                                 (image_dir), shell=True, stdout=subprocess.PIPE))
    for f in ['Dockerfile']:
        with open("%s/%s" % (image_dir, f), 'w') as ff:
            with open("%s/%s" % (image_template_dir, f), 'r') as fff:
                ff.write(e.from_string(fff.read()).render(
                    {'namespace': config['NAMESPACE']}))

    copy_authorized_keys(image_dir)
    copy_rc_files(image_dir)

    return get_image_dir(image_id)


def delete_image_dir(image_id):
    image_dir = get_image_dir(image_id)
    print_popen(subprocess.Popen("rm -rf %s" %
                                 (image_dir), shell=True, stdout=subprocess.PIPE))


def read_popen(p):
    try:
        output = [l.strip() for l in iter(p.stdout.readline, b'')]
        p.communicate()
        return '\n'.join(output)
    except Exception as e:
        print e


def print_popen(p):
    try:
        for l in iter(p.stdout.readline, b''):
            print l.strip()

        p.communicate()

    except Exception as e:
        print e


@roles('localhost')
def build_image(image_id):
    #import pdb; pdb.set_trace()
    if check_environment():
        try:
            image_dir = init_image_dir(image_id)

            image_exists = read_popen(subprocess.Popen("sudo docker images | grep '%s'" %
                                         (get_imagename(image_id)), shell=True, stdout=subprocess.PIPE))
            if not image_exists: 		

            	print_popen(subprocess.Popen("cd %s; sudo docker build -t %s ." %
                                         (image_dir, get_imagename(image_id)), shell=True, stdout=subprocess.PIPE))
            	delete_image_dir(image_id)
            else: 
            	print "duplicate image %s " % get_imagename(image_id)

            # delete_rc_files(image_dir)
            # delete_authorized_keys(image_dir)
        except Exception as e:
            print e


@roles('localhost')
def build_container(container_id):
    #import pdb; pdb.set_trace()
    hostname = get_hostname(container_id)
    image = get_imagename(config['CONTAINERS'][container_id]['image'])

    if check_environment():
        try:

            container_exists = read_popen(subprocess.Popen("sudo docker ps -a | grep '%s'" %
                                         (get_hostname(container_id)), shell=True, stdout=subprocess.PIPE))
            if not container_exists: 		
                #print_popen(subprocess.Popen("docker run -d --hostname %s --name %s -it --cpuset-cpus=%s %s /bin/bash" %
                #                         (hostname, hostname, config['CONTAINERS'][container_id].get('cpuset', 0-3), image), shell=True, stdout=subprocess.PIPE))
                print_popen(subprocess.Popen("docker run -d --hostname %s --name %s -it %s /bin/bash" %
                                         (hostname, hostname, image), shell=True, stdout=subprocess.PIPE))
            else:
                print_popen(subprocess.Popen("docker start %s" % (hostname), shell=True, stdout=subprocess.PIPE))

            	print "duplicate container %s " % get_hostname(container_id)
        except Exception as e:
            print e


def get_container_ids():
    return py_.sort_by(config['CONTAINERS'].keys(), lambda v: config['CONTAINERS'][v].get('order', -1))

def get_image_ids():
    return list(set([config['CONTAINERS'][k]['image'] for k in config['CONTAINERS'].keys()]))



def get_imagename(image_id):
    return string.Template(config['TEMPLATE']['IMAGENAME']).safe_substitute(image_id=image_id, namespace=config['NAMESPACE'])


def get_image_template_dir(image_id):
    return string.Template(config['TEMPLATE']['IMAGE_TEMPLATE_DIR']).safe_substitute(image_id=image_id)


def get_image_dir(image_id):
    return string.Template(config['TEMPLATE']['IMAGE_DIR']).safe_substitute(image_id=image_id, namespace=config['NAMESPACE'])


def get_hostname(container_id):
    return string.Template(config['TEMPLATE']['HOSTNAME']).safe_substitute(container_id=container_id, namespace=config['NAMESPACE'])


@roles('localhost')
def build_images(image_ids = get_image_ids()):
    for image_id in image_ids: 
        build_image(image_id)


@roles('localhost')
def build_init_images():
    #import pdb; pdb.set_trace()
    build_image('baseimage-16.04')
    build_image('openjdk-8-jdk')


@roles('localhost')
def build_containers(*args, **kwargs):
    if len(args) != 0: 
        container_ids = list(args)
    else: 
        container_ids = get_container_ids()


    for container_id in container_ids:
        build_container(container_id)



@roles('localhost')
def build_hostnames(*args, **kwargs):
    if len(args) != 0: 
        container_ids = list(args)
    else: 
        container_ids = get_container_ids()

    print_popen(subprocess.Popen("cp /etc/hosts /etc/hosts.bak",
                                 shell=True, stdout=subprocess.PIPE))

    for container_id in container_ids:
        try:
            #import pdb; pdb.set_trace()
            ip = get_ip(container_id)
            hostname = get_hostname(container_id)
            p = subprocess.Popen("cat /etc/hosts | grep -v '^$' | grep -v %s " %
                                 (hostname), shell=True, stdout=subprocess.PIPE)

            hosts_list = "\n".join([read_popen(p), "%s %s" % (ip, hostname)])

            with open('/etc/hosts', 'w') as f:
                f.write(hosts_list)

        except Exception as e:
            print e

    # print "\n".join(["%s %s" % (get_ip(container_id), get_hostname(container_id)) for container_id in container_ids])
    # return "\n".join(["%s %s" % (get_ip(container_id),
    # get_hostname(container_id)) for container_id in container_ids])


@roles('localhost')
def inspect_containers(container_ids=get_container_ids()):
    #import pdb; pdb.set_trace()
    for container_id in container_ids:
        container_info = inspect_container(container_id)

        #env.roledefs[container_id] = container_info['NetworkSettings']['IPAddress']


def get_ip(container_id):
    try:
        return str(inspect_container(container_id)['NetworkSettings']['IPAddress'])
    except Exception as e:
        return "0.0.0.0"


def inspect_container(container_id):
    #import pdb; pdb.set_trace()
    if check_environment():
        try:
            p = subprocess.Popen("docker ps -a | grep %s " %
                                 (get_hostname(container_id)), shell=True, stdout=subprocess.PIPE)
            container_uuid = re.split('\s+', read_popen(p))[0]

            p = subprocess.Popen("docker inspect %s" % (
                container_uuid), shell=True, stdout=subprocess.PIPE)

            return json.loads(read_popen(p))[0]

        except Exception as e:
            return {}

@roles('localhost')
def stop_containers(): 
    if check_environment():
        for container_id in get_container_ids():
            try:
                p = subprocess.Popen("docker ps -a | grep %s " %
                                     (get_hostname(container_id)), shell=True, stdout=subprocess.PIPE)
                container_uuid = re.split('\s+', read_popen(p))[0]
                if container_uuid:
                    read_popen(subprocess.Popen("docker stop %s" % (
                        container_uuid), shell=True, stdout=subprocess.PIPE))
            except Exception as e:
                pass



@roles('localhost')
def cleanup_containers(*args, **kwargs):
    #import pdb; pdb.set_trace()
    if len(args) != 0: 
        container_ids = list(args)
    else: 
        container_ids = get_container_ids()
    if check_environment():
        for container_id in container_ids: 
            try:
                p = subprocess.Popen("docker ps -a | grep %s " %
                                     (get_hostname(container_id)), shell=True, stdout=subprocess.PIPE)
                container_uuid = re.split('\s+', read_popen(p))[0]
                if container_uuid:
                    read_popen(subprocess.Popen("docker stop %s" % (
                        container_uuid), shell=True, stdout=subprocess.PIPE))
                    print_popen(subprocess.Popen("docker rm %s" % (
                        container_uuid), shell=True, stdout=subprocess.PIPE))
            except Exception as e:
                pass


@roles('localhost')
def cleanup_images(zombie = False):
    #import pdb; pdb.set_trace()
    if check_environment():
        for image_id in get_image_ids() + ['baseimage']:
            try:
                print_popen(subprocess.Popen("docker rmi %s" % (
                    get_imagename(image_id)), shell=True, stdout=subprocess.PIPE))
            except Exception as e:
                pass
        try:
            if zombie: 
                print_popen(subprocess.Popen("docker rmi $(docker images | grep '^<none>' | awk '{print $3}')", 
                    shell=True, 
                    stdout=subprocess.PIPE))
        except Exception as e:
            pass

def get_container_id(hostname): 
    regex = r"([^\.]+)%s$" % string.Template(config['TEMPLATE']['HOSTNAME']).safe_substitute(container_id="", namespace=config['NAMESPACE'])

    if re.search(regex, hostname): 
        return re.search(regex, hostname).group(1)
    else: 
        return ""

@roles('containers')
def build_ssh_keys(): 
    if check_environment():
        if not exists('~/.ssh/id_rsa'):
            run("ssh-keygen -f ~/.ssh/id_rsa -t rsa -N ''") 
        else: 
            pass
            #run('cat .ssh/id_rsa.pub')

@roles('localhost')
def populate_ssh_keys(): 
    if check_environment():
        ssh_pub_keys = [read_popen(subprocess.Popen("ssh %s 'cat ~/.ssh/id_rsa.pub'" % (get_hostname(container_id)), shell=True, stdout=subprocess.PIPE)) for container_id in get_container_ids()]

        for container_id in get_container_ids(): 
            authorized_keys = read_popen(subprocess.Popen("ssh %s 'cat ~/.ssh/authorized_keys'" % get_hostname(container_id), shell=True, stdout=subprocess.PIPE)).split('\n')
            for key in list(set(authorized_keys + ssh_pub_keys)):
                print_popen(subprocess.Popen("echo %s | ssh %s 'cat >> /tmp/authorized_keys'" % (key, get_hostname(container_id)), shell=True, stdout=subprocess.PIPE)) 

            read_popen(subprocess.Popen("ssh %s 'cat /tmp/authorized_keys | sort | uniq > ~/.ssh/authorized_keys'" % (get_hostname(container_id)), shell=True, stdout=subprocess.PIPE))

def get_container_id(hostname): 
    regex = r"([^\.]+)%s$" % string.Template(config['TEMPLATE']['HOSTNAME']).safe_substitute(container_id="", namespace=config['NAMESPACE'])

    if re.search(regex, hostname): 
        return re.search(regex, hostname).group(1)
    else: 
        return ""


@roles('containers')
def clone():
    #import pdb; pdb.set_trace()
    container_id = get_container_id(env.host_string)
    if container_id in config['CONTAINERS'].keys(): 
        if config['CONTAINERS'][container_id]['type'] == 'ngla': 
            if not exists('ngla-stable'):
                #run("GIT_SSL_NO_VERIFY=true git clone http://asds-server.nec-labs.com:8080/ngla/ngla-stable.git")
                run("GIT_SSL_NO_VERIFY=true git clone %s" % config['REPOSITORY']['URL'])

            with cd(config['REPOSITORY']['NAME']):
                run("GIT_SSL_NO_VERIFY=true git pull")
                run("GIT_SSL_NO_VERIFY=true git checkout %s" % config['REPOSITORY']['BRANCH'])
                run("bash config.sh")
                run("bash install.sh")


@roles('containers')
def populate_hostnames(container_ids=get_container_ids()):
    #import pdb; pdb.set_trace()
    run("cat /etc/hosts | grep -v '^$' | grep -v '^#' > /etc/hosts.bak")

    for container_id in container_ids:
        try:
            ip = get_ip(container_id)
            hostname = get_hostname(container_id)
            comment('/etc/hosts.bak', '^.+%s' % hostname)
            append('/etc/hosts.bak', "%s %s" % (ip, hostname))
        except Exception as e:
            pass

    run("cat /etc/hosts.bak | grep -v '^#' > /etc/hosts")

for container_id in get_container_ids(): 
    env.roledefs['containers'] = [get_hostname(container_id) for container_id in get_container_ids()]


def runbg(cmd, before=None, sockname="dtach", use_sudo=False):
    """
    :param cmd: The command to run
    :param output_file: The file to send all of the output to.
    :param before: The command to run before the dtach. E.g. exporting
                   environment variable
    :param sockname: The socket name to use for the temp file
    :param use_sudo: Whether or not to use sudo
    """
    #import pdb; pdb.set_trace()
    if not exists("/usr/bin/dtach"):
        sudo("apt-get install dtach")
    if before:
        cmd = "{}; dtach -n `mktemp -u /tmp/{}.XXXX` {}".format(
            before, sockname, cmd)
    else:
        cmd = "dtach -n `mktemp -u /tmp/{}.XXXX` {}".format(sockname, cmd)
    if use_sudo:
        return sudo(cmd)
    else:
        return run(cmd)


if __name__ == '__main__':
    print '\n'.join([
        'fab -f %s -R localhost build_init_images' % sys.argv[0],
        'fab -f %s -R localhost build_images' % sys.argv[0],
        'fab -f %s -R localhost build_containers' % sys.argv[0],
        'fab -f %s -R localhost build_hostnames' % sys.argv[0],
        'fab -u root -f %s -R containers populate_hostnames' % sys.argv[0],
        'fab -u root -f %s -R containers build_ssh_keys' % sys.argv[0],
        'fab -u root -f %s -R containers populate_ssh_keys' % sys.argv[0],
    ])
